﻿
namespace Agenda.Domain
{
    using System.Data.Entity;
    public class DataContext : DbContext
    {
        public DataContext() : base("DefaultConnection")
        {
        }
        public DbSet<Registry> Registries { get; set; }

        public DbSet<Data> Data { get; set; }
    }
}
