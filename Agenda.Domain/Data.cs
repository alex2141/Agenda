﻿

namespace Agenda.Domain
{
    using Newtonsoft.Json;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Data
    {
        [Key]
        public int DataID { get; set; }
        public int RegistryId { get; set; }
        [Required(ErrorMessage = "The Field {0} is Required.")]
        [MaxLength(50, ErrorMessage = "The field {0} only can contain {1} characters lenght.")]
        [Index("Data_Description_Index", IsUnique = true)]
        [DataType(DataType.MultilineText)]
        public string Observation { get; set; }
        [Display(Name = "Contact Id?")]
        public int ContactId { get; set;}
        public string  Phone { get; set; }
        public string Address { get; set; }
        public String Celphone { get; set; }
        public string Email { get; set; }
        [DataType(DataType.Date)]
        public DateTime Birthdate { get; set; }
        [Display(Name ="Last Actualization")]
        [DataType(DataType.Date)]
        public DateTime LastActualitation{ get; set; }
        public string Image { get; set; }
        [Display(Name = "Is Active?")]
        public bool IsActive { get; set; }
        [JsonIgnore]
        public virtual Registry Registry { get; set; }
    }
}
