﻿namespace Agenda.Domain
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Registry
    {
        [Key]
        public int RegistryId
        { get; set; }
        [Required(ErrorMessage = "The Field {0} is Required.")]
        [MaxLength(200, ErrorMessage = "The field {0} only can contain {1} characters lenght.")]
        [Index("Registry_Description_Index", IsUnique = false)]
        [Display(Name = "Login Contact")]
        public string Description { get; set; }
        [JsonIgnore]
        public virtual ICollection<Data> Datum { get; set; }

    }
}
