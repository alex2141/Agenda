﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Agenda.Backend.Models;
using Agenda.Domain;

namespace Agenda.Backend.Controllers
{
    public class RegistriesController : Controller
    {
        private DataContextLocal db = new DataContextLocal();

        // GET: Registries
        public async Task<ActionResult> Index()
        {
            return View(await db.Registries.ToListAsync());
        }

        // GET: Registries/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Registry registry = await db.Registries.FindAsync(id);
            if (registry == null)
            {
                return HttpNotFound();
            }
            return View(registry);
        }

        // GET: Registries/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Registries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "RegistryId,Description")] Registry registry)
        {
            if (ModelState.IsValid)
            {
                db.Registries.Add(registry);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(registry);
        }

        // GET: Registries/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Registry registry = await db.Registries.FindAsync(id);
            if (registry == null)
            {
                return HttpNotFound();
            }
            return View(registry);
        }

        // POST: Registries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "RegistryId,Description")] Registry registry)
        {
            if (ModelState.IsValid)
            {
                db.Entry(registry).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(registry);
        }

        // GET: Registries/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Registry registry = await db.Registries.FindAsync(id);
            if (registry == null)
            {
                return HttpNotFound();
            }
            return View(registry);
        }

        // POST: Registries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Registry registry = await db.Registries.FindAsync(id);
            db.Registries.Remove(registry);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
