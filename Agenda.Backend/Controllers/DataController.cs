﻿namespace Agenda.Backend.Controllers
{
    using System.Data.Entity;
    using System.Threading.Tasks;
    using System.Net;
    using System.Web.Mvc;
    using Agenda.Backend.Models;
    using Agenda.Domain;
    using Agenda.Backend.Helpers;
    using System;

    [Authorize]
    public class DataController : Controller
    {
        private DataContextLocal db = new DataContextLocal();

        // GET: Data
        public async Task<ActionResult> Index()
        {
            var data = db.Data.Include(d => d.Registry);
            return View(await data.ToListAsync());
        }

        // GET: Data/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var data = await db.Data.FindAsync(id);
            if (data == null)
            {
                return HttpNotFound();
            }
            return View(data);
        }

        // GET: Data/Create
        public ActionResult Create()
        {
            ViewBag.RegistryId = new SelectList(db.Registries, "RegistryId", "Description");
            return View();
        }

        // POST: Data/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(DataView view)
        {
            if (ModelState.IsValid)
            {
                var pic = string.Empty;
                var folder = "~/Content/Images";

                if (view.ImageFile != null)
                {
                    pic = FilesHelper.UploadPhoto(view.ImageFile, folder);
                    pic = string.Format("{0}/{1}", folder, pic);
                }
                var data = ToData(view);
                data.Image = pic;
                db.Data.Add(data);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");


            }

            ViewBag.RegistryId = new SelectList(db.Registries, "RegistryId", "Description",view.RegistryId);
            return View(view);
        }

        private Data ToData(DataView view)
        {
            return new Data
            {
                
                DataID = view.DataID,
                RegistryId = view.RegistryId,
                Observation = view.Observation,
                ContactId = view.ContactId,
                Phone = view.Phone,
                Address = view.Address,
                Celphone = view.Celphone,
                Email = view.Email,
                Image = view.Image,
                IsActive = view.IsActive,
                Birthdate= view.Birthdate,
                LastActualitation= view.LastActualitation,

    };
        }

        // GET: Data/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Data data = await db.Data.FindAsync(id);
            if (data == null)
            {
                return HttpNotFound();
            }
            ViewBag.RegistryId = new SelectList(db.Registries, "RegistryId", "Description", data.RegistryId);
            var view = ToView(data);
            return View(view);
        }

        private DataView ToView(Data data)
        {
            return new DataView
            {

                DataID = data.DataID,
                RegistryId = data.RegistryId,
                Observation = data.Observation,
                ContactId = data.ContactId,
                Phone = data.Phone,
                Address = data.Address,
                Celphone = data.Celphone,
                Email = data.Email,
                Image = data.Image,
                IsActive = data.IsActive,
                Birthdate = data.Birthdate,
                LastActualitation = data.LastActualitation,

            };
        }

        // POST: Data/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(DataView view)
        {
            if (ModelState.IsValid)
            {
                var pic = view.Image;
                var folder = "~/Content/Images";

                if (view.ImageFile != null)
                {
                    pic = FilesHelper.UploadPhoto(view.ImageFile, folder);
                    pic = string.Format("{0}/{1}", folder, pic);
                }
                var data = ToData(view);
                data.Image = pic;
                db.Entry(data).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.RegistryId = new SelectList(db.Registries, "RegistryId", "Description", view.RegistryId);
            return View(view);
        }

        // GET: Data/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Data data = await db.Data.FindAsync(id);
            if (data == null)
            {
                return HttpNotFound();
            }
            return View(data);
        }

        // POST: Data/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Data data = await db.Data.FindAsync(id);
            db.Data.Remove(data);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
