﻿namespace Agenda.Backend.Models
{
    using Domain;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Web;

    [NotMapped]
    public class RegistryView : Registry
	{
        [Display(Name = "Photo")]
        public HttpPostedFileBase ImageFile { get; set; }
    }
}