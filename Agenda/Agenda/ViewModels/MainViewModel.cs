﻿using Agenda.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Agenda.ViewModels
{
    using Models;
    public class MainViewModel
    {
        #region Properties
        public LoginViewModel Login
        {
            get;
            set;
        }
        public RegistryViewModel Registry
        {
            get;
            set;
        }
        public TokenResponse Token
        {
            get;
            set;

        }
        #endregion

        #region Constructors
        public MainViewModel()
        {
            Login = new LoginViewModel();
        }
        #endregion
        #region Sigleton
        static MainViewModel instance;

        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }

            return instance;
        }
        #endregion
    }
}

