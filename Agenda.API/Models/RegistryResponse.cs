﻿namespace Agenda.API.Models
{
    using System.Collections.Generic;

    public class RegistryResponse
    {
        public int RegistryId { get; set; }
       
        public string Description { get; set; }
   
        public virtual List<DataResponse> Datum { get; set; }
    }
}