﻿namespace Agenda.API.Models
{
    using System;
    public class DataResponse
    {
        public int DataID { get; set; }
       
        public string Observation { get; set; }
     
        public int ContactId { get; set; }

        public string Phone { get; set; }

        public string Address { get; set; }

        public String Celphone { get; set; }

        public string Email { get; set; }
    
        public DateTime Birthdate { get; set; }
 
        public DateTime LastActualitation { get; set; }

        public string Image { get; set; }
       
        public bool IsActive { get; set; }


     }
}