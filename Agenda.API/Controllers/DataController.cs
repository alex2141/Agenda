﻿namespace Agenda.API.Controllers
{
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Http.Description;
    using Agenda.Domain;

    [Authorize]
    public class DataController : ApiController
    {
        private DataContext db = new DataContext();

        // GET: api/Data
        public IQueryable<Data> GetData()
        {
            return db.Data;
        }

        // GET: api/Data/5
        [ResponseType(typeof(Data))]
        public async Task<IHttpActionResult> GetData(int id)
        {
            Data data = await db.Data.FindAsync(id);
            if (data == null)
            {
                return NotFound();
            }

            return Ok(data);
        }

        // PUT: api/Data/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutData(int id, Data data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != data.DataID)
            {
                return BadRequest();
            }

            db.Entry(data).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DataExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Data
        [ResponseType(typeof(Data))]
        public async Task<IHttpActionResult> PostData(Data data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Data.Add(data);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = data.DataID }, data);
        }

        // DELETE: api/Data/5
        [ResponseType(typeof(Data))]
        public async Task<IHttpActionResult> DeleteData(int id)
        {
            Data data = await db.Data.FindAsync(id);
            if (data == null)
            {
                return NotFound();
            }

            db.Data.Remove(data);
            await db.SaveChangesAsync();

            return Ok(data);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DataExists(int id)
        {
            return db.Data.Count(e => e.DataID == id) > 0;
        }
    }
}