﻿
namespace Agenda.API.Controllers
{
    using Agenda.API.Models;
    using Agenda.Domain;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Http.Description;

    [Authorize]
    public class RegistriesController : ApiController
    {
        private DataContext db = new DataContext();

        // GET: api/Registries
        public async Task<IHttpActionResult> GetRegistries()
        {
           var registry = await db.Registries.ToListAsync();
            var registryResponse = new List<RegistryResponse>();

            foreach (var registri in registry)
            {
                var dataResponse = new List<DataResponse>();
                foreach (var data in registri.Datum)
                {
                    dataResponse.Add(new DataResponse
                    {
                      Observation = data.Observation,
                      Image = data.Image,
                      IsActive = data.IsActive,
                      LastActualitation = data.LastActualitation,
                      Address= data.Address,
                      Celphone = data.Celphone,
                      Phone= data.Phone,
                      Birthdate = data.Birthdate,
                      Email = data.Email,
                      ContactId = data.ContactId,
                      DataID = data.DataID,

                    });
                }
                registryResponse.Add (new RegistryResponse
                { 
                    RegistryId = registri.RegistryId,
                    Description = registri.Description,
                    Datum = dataResponse,
              });
            }
            return Ok(registryResponse);
        }

        // GET: api/Registries/5
        [ResponseType(typeof(Registry))]
        public async Task<IHttpActionResult> GetRegistry(int id)
        {
            Registry registry = await db.Registries.FindAsync(id);
            if (registry == null)
            {
                return NotFound();
            }

            return Ok(registry);
        }

        // PUT: api/Registries/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutRegistry(int id, Registry registry)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != registry.RegistryId)
            {
                return BadRequest();
            }

            db.Entry(registry).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RegistryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Registries
        [ResponseType(typeof(Registry))]
        public async Task<IHttpActionResult> PostRegistry(Registry registry)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Registries.Add(registry);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = registry.RegistryId }, registry);
        }

        // DELETE: api/Registries/5
        [ResponseType(typeof(Registry))]
        public async Task<IHttpActionResult> DeleteRegistry(int id)
        {
            Registry registry = await db.Registries.FindAsync(id);
            if (registry == null)
            {
                return NotFound();
            }

            db.Registries.Remove(registry);
            await db.SaveChangesAsync();

            return Ok(registry);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RegistryExists(int id)
        {
            return db.Registries.Count(e => e.RegistryId == id) > 0;
        }
    }
}